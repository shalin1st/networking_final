#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <conio.h>
#include <algorithm>
#include "buffer.h"
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#define DEFAULT_BUFLEN 512

class lobbypropert
{
public:
	lobbypropert();
	~lobbypropert();
	void getGameModes();
	bool lobbyvalidity(std::string map, std::string gameMode);
	void getAllDetails();
	void getMaps();
private:
	std::string *gameModes;
	std::string *maps;
	bool isPresentInArray(std::string *strArray, std::string item);
	bool isStringEquals(std::string a, std::string b);
};














bool terminate1 = false;
buffer sendBuffer(512);
buffer recvBuffer(512);
std::vector<std::string> rooms;
bool processfunc(std::string userMessage);
bool connectServer(std::string userMessage);
SOCKET ConnectSocket = INVALID_SOCKET;
char recvbuf[DEFAULT_BUFLEN];
int recvbuflen = DEFAULT_BUFLEN;
bool joinLobby(std::string);
bool leaveLobby(std::string);
bool checkRoom(std::string);
bool sendMessage(std::string, std::string);
bool registerUser(std::string);
bool authenticateUser(std::string);
bool logoutUser(std::string);
void recvMessage(); int iResult;
bool refreshLobbyBrowser(std::string);
bool createLobby(std::string);


std::string userIn = "";
char keyIn;
std::string gSessionToken = "";
bool gIsLoggedIn = false;
std::string gLobbyJoined = "";
lobbypropert *lobbyDetails;

int __cdecl main(int argc, char **argv)
{
	rooms.resize(20);
	std::cout << "All Available Commands : \n";
	std::cout << "/connect   [Ip Address] [Port] -- To connect to a game server \n";
	
	do
	{
		if (_kbhit())
		{
			char keyIn;
			keyIn = _getch();
			if (keyIn == '\r')
			{
				std::cout << '\r';
				for (int i = 0; i < userIn.length(); i++)
					std::cout << ' ';
				std::cout << '\r';
				terminate1 = !(processfunc(userIn));
				userIn = "";
			}
			else if (keyIn == 127 || keyIn == 8) 
			{
				userIn = userIn.substr(0, userIn.length() - 1);
				std::cout << keyIn << ' ' << keyIn;
			}
			else
			{
				userIn += keyIn;
				std::cout << keyIn;
			}
		}

		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0)
		{
			std::string getData = "";
			for (int i = 0; i < 4; i++)
				getData += recvbuf[i];
			recvBuffer.writeString(getData);
			int recvSize = recvBuffer.readInt32BE();
			for (int i = 4; i < recvSize; i++)
				getData += recvbuf[i];
			recvBuffer.writeString(getData);

			recvMessage();

		}
		else if (iResult == 0)
			
			continue;
	} while (!terminate1);

	
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}



void showAvailableCommands() {
	
	printf("/register  username password \n");
	printf("/login     username password \n");;
	printf("/logout  \n");
	printf("/viewlobby \n");
	printf("/refresh   -- To refresh the game lobby list \n");
	printf("/create    [mapName] [lobbyName] [gameMode] [maxPlayers]  -- To create a new lobby \n");
	printf("/join      [lobbyName] -- To join a new lobby \n");
	printf("/leave     [lobbyName] -- To join a new lobby \n");
	printf("/exit      -- To exit the client \n");
}

std::vector<std::string> explodeString(std::string inputString, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(inputString);

	if (inputString != "") {
		for (std::string token; std::getline(iss, token, delim); )
		{
			if (!token.empty() && token != "")
				result.push_back(std::move(token));
		}
	}

	return result;
}

bool processfunc(std::string userMessage)
{

	std::vector<std::string> result;
	std::istringstream iss(userMessage);
	std::string s;
	iss >> s;
	if (s == "/connect") {
		if (::gSessionToken == "")
			connectServer(userMessage);
		else
			std::cout << "Already connected to a game server !\n";
	}
	else if (s == "/register")
	{
		if (::gIsLoggedIn == false && ::gSessionToken != "")
			registerUser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn)
			std::cout << "Already logged in ! Logout first to register.\n";
	}
	else if (s == "/login")
	{
		if (::gIsLoggedIn == false && ::gSessionToken != "")
			authenticateUser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn)
			std::cout << "Already logged in ! Logout first to login again.\n";
	}
	else if (s == "/logout") {
		if (::gIsLoggedIn && ::gSessionToken != "")
			logoutUser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to logout .\n";
	}
	else if (s == "/refresh" || s == "/viewlobby") {
		if (::gIsLoggedIn && ::gLobbyJoined == "")
			refreshLobbyBrowser(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to view the lobby browser !\n";
		else if (::gIsLoggedIn && ::gLobbyJoined != "")
			std::cout << "Please leave the current lobby to view the game browser !\n";
	}
	else if (s == "/create") {
		if (::gIsLoggedIn && ::gLobbyJoined == "")
			createLobby(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to create the lobby !\n";
		else if (::gIsLoggedIn && ::gLobbyJoined != "")
			std::cout << "Please leave the current lobby to create the lobby !\n";
	}
	else if (s == "/join") {
		if (::gIsLoggedIn && ::gLobbyJoined == "")
			joinLobby(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to join the lobby !\n";
		else if (::gIsLoggedIn && ::gLobbyJoined != "")
			std::cout << "Already joined the lobby " + gLobbyJoined + "! Please leave first to join another \n";
	}
	else if (s == "/send")
	{
		iss >> s;
		if (checkRoom(s))
			sendMessage(s, userMessage);
		else
			std::cout << "Not a part of the room " << s << '\n';
	}
	else if (s == "/leave") {
		if (::gIsLoggedIn && ::gLobbyJoined != "")
			leaveLobby(userMessage);
		else if (::gSessionToken == "")
			std::cout << "Please connect to a game server first \n";
		else if (::gIsLoggedIn == false)
			std::cout << "Please login first to leave a lobby !\n";
		else if (::gIsLoggedIn && ::gLobbyJoined == "")
			std::cout << "You are not present in any lobby to leave ! Please join a lobby first. \n";
	}
	else if (s == "/exit") {
		::gSessionToken = "";
		::gLobbyJoined = "";
		::gIsLoggedIn = false;
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	else
	{
		std::cout << "Invalid command. Please choose one of the commands to execute : \n ";
		showAvailableCommands();
	}
	return 1;
}

bool validateIpAddress(std::string &ipAddress)
{
	struct sockaddr_in sa;
	int result = inet_pton(AF_INET, ipAddress.c_str(), &(sa.sin_addr));
	if (result == 1)
		return true;
	else
		return false;
}

bool connectServer(std::string input)
{
	using namespace std::literals;
	std::istringstream iss(input);
	std::string ip, port;
	iss >> ip; 
	iss >> ip;
	iss >> port;


	for (int i = 0; i < ip.length(); i++)
		if (ip[i] == ' ')
			ip[i] = '_';


	bool isValidIp = validateIpAddress(ip);
	if (!isValidIp)
		return -1;


	WSADATA wsaData;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 0;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	
	iResult = getaddrinfo(ip.c_str(), port.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 0;
	}


	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

	
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 0;
		}

	
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 0;
	}

	u_long iMode = 1;
	iResult = ioctlsocket(ConnectSocket, FIONBIO, &iMode);
	if (iResult != 0) {
		printf("Making socket non-blocking failed : %d\n", iResult);
		return 0;
	}

	return 1;
}

bool authenticateUser(std::string input)
{
	std::istringstream iss(input);
	std::string email, password;
	iss >> email; 
	iss >> email;
	iss >> password;

	int messageID = 2;
	int emailLength = email.length();
	int passwordLength = password.length();
	int sessionTokenLength = gSessionToken.length();
	int packetLength = emailLength + passwordLength + sessionTokenLength + 20;

	std::cout << gSessionToken << std::endl;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(gSessionToken);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(email);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(password);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool registerUser(std::string input)
{
	std::istringstream iss(input);
	std::string email, password;
	iss >> email; 
	iss >> email;
	iss >> password;

	int messageID = 4;
	int emailLength = email.length();
	int passwordLength = password.length();
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = emailLength + passwordLength + sessionTokenLength + 20;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(email);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(password);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool logoutUser(std::string input) {
	int messageID = 6;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = sessionTokenLength + 12;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool refreshLobbyBrowser(std::string input) {
	int messageID = 8;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = sessionTokenLength + 12;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool createLobby(std::string input) {
	std::istringstream iss(input);
	std::string map, lobbyName, gameMode, maxPlayers;
	iss >> map; 
	iss >> map;
	iss >> lobbyName;
	iss >> gameMode;
	iss >> maxPlayers;

	lobbyDetails = new lobbypropert();
	bool isValid = lobbyDetails->lobbyvalidity(map, gameMode);
	if (!isValid)
		return 1;

	int messageID = 10;
	int sessionTokenLength = ::gSessionToken.length();
	int mapLength = map.length();
	int lobbyNameLength = lobbyName.length();
	int gameModeLength = gameMode.length();
	int maxPlayersLength = maxPlayers.length();
	int packetLength = sessionTokenLength + mapLength + lobbyNameLength + gameModeLength + maxPlayersLength + 28;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(mapLength);
	sendBuffer.writeString(map);
	sendBuffer.writeInt32BE(lobbyNameLength);
	sendBuffer.writeString(lobbyName);
	sendBuffer.writeInt32BE(gameModeLength);
	sendBuffer.writeString(gameMode);
	sendBuffer.writeInt32BE(maxPlayersLength);
	sendBuffer.writeString(maxPlayers);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool joinLobby(std::string input)
{
	input.erase(0, 6); 

	for (int i = 0; i < input.length(); i++)
		if (input[i] == ' ')
			input[i] = '_';

	int lobbyLength = input.length();
	int messageId = 12;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = lobbyLength + sessionTokenLength + 16;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageId);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool leaveLobby(std::string input)
{
	input.erase(0, 7);

	for (int i = 0; i < input.length(); i++)
		if (input[i] == ' ')
			input[i] = '_';

	int lobbyLength = input.length();
	int messageId = 14;
	int sessionTokenLength = ::gSessionToken.length();
	int packetLength = lobbyLength + sessionTokenLength + 16;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageId);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(::gSessionToken);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

bool checkRoom(std::string input)
{
	for (std::vector<std::string>::iterator it = rooms.begin(); it != rooms.end(); ++it)
		if (*it == input)
			return 1;

	return 0;
}

bool sendMessage(std::string room, std::string input)
{
	int eraseHead = room.length() + 7; 
	input.erase(0, eraseHead);

	int roomLength = room.length();
	int messageLength = input.length();
	int messageID = 3;
	int packetLength = roomLength + messageLength + 16;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(roomLength);
	sendBuffer.writeString(room);
	sendBuffer.writeInt32BE(messageLength);
	sendBuffer.writeString(input);

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}

	return 1;
}

void recvMessage()
{
	int packetLength = recvBuffer.readInt32BE();
	int messageID = recvBuffer.readInt32BE();
	if (messageID == 0)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		::gSessionToken = recvBuffer.readString(sessionTokenLength);
		std::cout << "You have been successfully connected to the game server. Please login or register to access the lobbies !\n";
		showAvailableCommands();
	}
	else if (messageID == 1)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (messageID == 2)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gIsLoggedIn = true;
		std::cout << message << "\n";
		lobbyDetails = new lobbypropert();
		lobbyDetails->getAllDetails();
		std::cout << "\n \n Game Lobby Browser \n";
		refreshLobbyBrowser("");
	}
	else if (messageID == 3)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (messageID == 4)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gIsLoggedIn = false;
		::gLobbyJoined = "";
		std::cout << message << "\n";
	}
	else if (messageID == 5)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gLobbyJoined = message;
		std::cout << "Created and joined the lobby " + message + "successfully." << "\n";
	}
	else if (messageID == 6)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::vector<std::string> msgData = explodeString(message, '||');
		::gLobbyJoined = msgData[0];
		std::cout << msgData[1] << "\n";
	}
	else if (messageID == 7)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		std::vector<std::string> msgData = explodeString(message, '||');
		::gLobbyJoined = "";
		std::cout << msgData[1] << "\n";
	}
	else if (messageID == 8)
	{
		int roomLength = recvBuffer.readInt32BE();
		std::string roomName = recvBuffer.readString(roomLength);
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int messageLength = recvBuffer.readInt32BE();
		std::string message = recvBuffer.readString(messageLength);
		::gLobbyJoined = "";
		std::cout << message << "\n";
		refreshLobbyBrowser("");
	}
	else if (messageID == 9)
	{
		int sessionTokenLength = recvBuffer.readInt32BE();
		std::string discard = recvBuffer.readString(sessionTokenLength);
		int rowCount = recvBuffer.readInt32BE();
		for (int i = 0; i < rowCount; i++) {
			for (int j = 1; j < 10; j++) {
				int tmpLength = 0; std::string tmpString = "";
				tmpLength = recvBuffer.readInt32BE();
				tmpString = recvBuffer.readString(tmpLength);
				std::cout << tmpString << " || ";
			}
			std::cout << "\n";
		}

	}
	return;
}


















/////////////////////////////////////////////////////////////

lobbypropert::lobbypropert()
{
	this->maps = new std::string[5]{ "NewYork","Boston", "London", "Berlin", "Moscow" };
	this->gameModes = new std::string[5]{ "FreeForAll","Team","CaptureTheFlag","DeathMatch","LastManStanding" };
}


lobbypropert::~lobbypropert()
{
}

void lobbypropert::getAllDetails() {
	std::cout << "For creating a lobby, you can use one of the maps &  game modes mentioned below : \n";
	this->getMaps();
	this->getGameModes();
}
void lobbypropert::getMaps() {
	std::cout << "Maps Available : \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->maps[i] << "\n";
	}
}
void lobbypropert::getGameModes() {
	std::cout << "Game Modes Available : \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->gameModes[i] << "\n";
	}
}

bool lobbypropert::lobbyvalidity(std::string map, std::string gameMode) {
	bool isMapValid = false;
	bool isGameModeValid = false;
	if (isPresentInArray(this->maps, map) == true)
		isMapValid = true;
	if (isPresentInArray(this->maps, map) == true)
		isGameModeValid = true;

	if (isMapValid && isGameModeValid)
		return true;
	if (isMapValid == false) {
		std::cout << "Map is invalid . Please use one of the following : \n ";
		this->getMaps();
	}
	if (isGameModeValid == false) {
		std::cout << "GameMode is invalid . Please use one of the following : \n ";
		this->getGameModes();
	}
	return false;
}

bool lobbypropert::isPresentInArray(std::string *strArray, std::string item) {
	for (int i = 0; i < strArray->size(); i++) {
		if (isStringEquals(strArray[i], item))
			return true;
	}
	return false;
};

bool lobbypropert::isStringEquals(std::string a, std::string b)
{
	unsigned int sz = a.size();
	if (b.size() != sz)
		return false;
	for (unsigned int i = 0; i < sz; ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}
